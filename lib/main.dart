import 'package:editz/home_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.amber),
      home: const HomeScreen(),
    );
  }
}

/*
edit screen

  stack widgets

    text overlay widget
      listener
        gesture
          animated builder
            textfield
*/