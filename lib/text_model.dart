import 'package:flutter/material.dart';

class TextModel {
  double fontSize;
  FontWeight fontWeight;
  Color color;
  TextAlign textAlign;

  TextModel(
      {required this.fontSize,
      required this.color,
      required this.fontWeight,
      required this.textAlign});
}
