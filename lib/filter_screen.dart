import 'dart:io';
import 'package:editz/edit_screen.dart';
import 'package:editz/filters.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:ui' as ui;

class FilterScreen extends StatefulWidget {
  final XFile imageFile;
  const FilterScreen({super.key, required this.imageFile});

  @override
  State<FilterScreen> createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  int selectedIndex = -1;

  final GlobalKey _globalKey = GlobalKey();

  late Uint8List imageDate;


  @override
  void initState() {
    super.initState();
    convetImagetoBytes();
  }

  Future<void> convetImagetoBytes() async {
    imageDate = await widget.imageFile.readAsBytes();
    setState(() {});
  }

  Future<void> convertWidgetToImage() async {
    RenderRepaintBoundary? repaintBoundary =
        _globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    //  RenderRepaintBoundary repaintBoundary = RenderRepaintBoundary(child: renderObject);
    ui.Image boxImage = await repaintBoundary.toImage(pixelRatio: 1.0);
    ByteData? byteData =
        await boxImage.toByteData(format: ui.ImageByteFormat.png);
    Uint8List uint8list = byteData!.buffer.asUint8List();
    imageDate = uint8list;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () => Navigator.pop(context), icon: Icon(Icons.close)),
        title: const Text("Apply Filter"),
        actions: [
          IconButton(
            onPressed: () async {
              await convertWidgetToImage();
              Navigator.of(context).pushReplacement(
                CupertinoPageRoute(
                    builder: (context) => EditScreen(
                          bgImage: imageDate,
                        )),
              );
            },
            icon: const Icon(Icons.done),
          )
        ],
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          // shrinkWrap: true,
          // primary: true,
          children: [
            RepaintBoundary(
              key: _globalKey,
              child: ColorFiltered(
                colorFilter: ColorFilter.matrix(filters[1]),
                child: Image.memory(
                  imageDate,
                  fit: BoxFit.cover,
                  height: MediaQuery.of(context).size.height * 0.7,
                  width: MediaQuery.of(context).size.width,
                ),
              ),
            ),
            SizedBox(height: 30),
            SizedBox(
              height: 90,
              child: ListView.builder(
                // shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: filters.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      selectedIndex = index;
                      convertWidgetToImage();
                      // setState(() {});
                    },
                    child: Container(
                      width: 90,
                      height: 50,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: selectedIndex == index
                            ? Border.all(color: Colors.yellowAccent, width: 3)
                            : null,
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: ColorFiltered(
                          colorFilter: ColorFilter.matrix(filters[index]),
                          child: Image.file(
                            File(widget.imageFile.path),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
