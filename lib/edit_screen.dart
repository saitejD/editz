import 'dart:io';
import 'dart:typed_data';
import 'package:editz/OverlayWidgets/image_overlay_widget.dart';
import 'package:editz/OverlayWidgets/text_overlay_widget.dart';
import 'package:editz/text_model.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

class EditScreen extends StatefulWidget {
  final Uint8List bgImage;
  const EditScreen({super.key, required this.bgImage});

  @override
  State<EditScreen> createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> {
  List<Widget> _stackWidgets = [];

  bool _showDeleteButton = false;
  bool _isDeleteButtonActive = false;

  bool showStickeers = false;

  final ImagePicker _picker = ImagePicker();
  final ScreenshotController _screenshotController = ScreenshotController();

  double fontSize = 40;

  Offset _offset = Offset.zero;

  TextModel textModel = TextModel(
      fontSize: 14,
      color: Colors.red,
      fontWeight: FontWeight.bold,
      textAlign: TextAlign.center);

  Key? widgetKey;

  @override
  void initState() {
    super.initState();
    // createFolder();
  }

  Future<void> createFolder() async {
    String folderName = "Editz";

    final Directory path = Directory(folderName);
    if ((await path.exists())) {
      print("path already exists");
    } else {
      path.create();
    }
  }

  saveToGallery() {
    _screenshotController.capture().then((Uint8List? value) {
      saveImage(value!);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Photo is saved to Gallery")));
    }).catchError((error) => print(error));
  }

  saveImage(Uint8List bytes) async {
    final date = DateTime.now()
        .toIso8601String()
        .replaceAll(".", "-")
        .replaceAll(":", "-");
    final name = "screenshot_$date";
    await requestPermission(Permission.storage);
    await ImageGallerySaver.saveImage(bytes, name: name);
  }

  requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,

      // appBar: AppBar(
      //   backgroundColor: Colors.transparent,
      //   title: Text("Edit Screen"),
      //   actions: [
      //     IconButton(
      //         onPressed: () {
      //           saveToGallery();
      //         },
      //         icon: const Icon(Icons.save_alt)),
      //   ],
      // ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.add),
      //   onPressed: () {
      //     setState(() {
      //       _stackWidgets.add(
      //         TextOverlayWidget(
      //           key: Key(_stackWidgets.length.toString()),

      //           onDragStart: () {
      //             if (!_showDeleteButton) {
      //               setState(() {
      //                 _showDeleteButton = true;
      //               });
      //             }
      //           },
      //           onDragUpdate: (Offset offset, key) {
      //             if (offset.dy > size.height - 100) {
      //               if (!_isDeleteButtonActive) {
      //                 setState(() {
      //                   _isDeleteButtonActive = true;
      //                 });
      //               }
      //             } else {
      //               if (_isDeleteButtonActive) {
      //                 setState(() {
      //                   _isDeleteButtonActive = false;
      //                 });
      //               }
      //             }
      //           },
      //           onDragEnd: (Offset offset, key) {
      //             if (_showDeleteButton) {
      //               setState(() {
      //                 _showDeleteButton = false;
      //               });
      //             }

      //             if (offset.dy > size.height - 100) {
      //               print(_stackWidgets.first.key);
      //               _stackWidgets.removeWhere((element) => element.key == key);
      //             }
      //           },
      //         ),
      //       );
      //     });
      //   },
      // ),
      body: WillPopScope(
        onWillPop: showPopup,
        child: SizedBox(
          height: size.height,
          width: size.width,
          // decoration: BoxDecoration(
          //   image: DecorationImage(
          //     image: FileImage(
          //       File(widget.bgImage.path),
          //     ),
          //     fit: BoxFit.cover,
          //   ),
          // ),
          child: Screenshot(
            controller: _screenshotController,
            child: Stack(
              children: [
                Image.memory(
                  widget.bgImage,
                  fit: BoxFit.cover,
                  height: size.height,
                  width: size.width,
                ),
                for (int i = 0; i < _stackWidgets.length; i++) _stackWidgets[i],
                if (_showDeleteButton)
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.all(30),
                      child: Icon(
                        Icons.delete,
                        size: _isDeleteButtonActive ? 40 : 28,
                        color:
                            _isDeleteButtonActive ? Colors.red : Colors.white,
                      ),
                    ),
                  ),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 50,
                      left: 10,
                      child: IconButton(
                        onPressed: () async {
                          bool res = await showPopup();
                          if (res) {
                            Navigator.pop(context);
                          }
                        },
                        icon: const Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 35,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 50,
                      right: 10,
                      child: IconButton(
                        onPressed: () {
                          saveToGallery();
                        },
                        icon: const Icon(
                          Icons.save_alt,
                          color: Colors.white,
                          size: 35,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 150,
                      right: 10,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              Key k = Key(_stackWidgets.length.toString());
                              setState(() {
                                _stackWidgets.add(
                                  TextOverlayWidget(
                                    key: k,
                                    onDragStart: () {
                                      if (!_showDeleteButton) {
                                        setState(() {
                                          _showDeleteButton = true;
                                        });
                                      }
                                    },
                                    onDragUpdate: (Offset offset, key) {
                                      if (offset.dy >
                                          MediaQuery.of(context).size.height -
                                              50) {
                                        if (!_isDeleteButtonActive) {
                                          setState(() {
                                            _isDeleteButtonActive = true;
                                          });
                                        }
                                      } else {
                                        if (_isDeleteButtonActive) {
                                          setState(() {
                                            _isDeleteButtonActive = false;
                                          });
                                        }
                                      }
                                    },
                                    onDragEnd: (Offset offset, key) {
                                      if (_showDeleteButton) {
                                        setState(() {
                                          _showDeleteButton = false;
                                        });
                                      }

                                      if (offset.dy >
                                          MediaQuery.of(context).size.height -
                                              50) {
                                        print(_stackWidgets.first.key);
                                        _stackWidgets.removeWhere(
                                            (element) => element.key == key);
                                      }
                                    },
                                    onTaped: (Key? key) {
                                      // setState(() {
                                      //   _offset = offset;
                                      // });
                                    },
                                  ),
                                );
                              });
                            },
                            icon: const Icon(
                              Icons.text_rotation_none_sharp,
                              color: Colors.white,
                              size: 35,
                            ),
                          ),
                          IconButton(
                            onPressed: () async {
                              Key k = Key(_stackWidgets.length.toString());
                              final XFile? image = await _picker.pickImage(
                                  source: ImageSource.gallery);
                              if (image != null) {
                                setState(() {
                                  _stackWidgets.add(
                                    ImageOverlayWidget(
                                      isAsset: false,
                                      photo: File(image.path),
                                      key: k,
                                      onDragStart: () {
                                        if (!_showDeleteButton) {
                                          setState(() {
                                            _showDeleteButton = true;
                                          });
                                        }
                                      },
                                      onDragUpdate: (Offset offset, key) {
                                        if (offset.dy > size.height - 50) {
                                          if (!_isDeleteButtonActive) {
                                            setState(() {
                                              _isDeleteButtonActive = true;
                                            });
                                          }
                                        } else {
                                          if (_isDeleteButtonActive) {
                                            setState(() {
                                              _isDeleteButtonActive = false;
                                            });
                                          }
                                        }
                                      },
                                      onDragEnd: (Offset offset, key) {
                                        if (_showDeleteButton) {
                                          setState(() {
                                            _showDeleteButton = false;
                                          });
                                        }

                                        if (offset.dy > size.height - 50) {
                                          print(_stackWidgets.first.key);
                                          _stackWidgets.removeWhere(
                                              (element) => element.key == key);
                                        }
                                      },
                                    ),
                                  );
                                });
                              }
                            },
                            icon: Icon(
                              Icons.add_photo_alternate,
                              color: Colors.white,
                              size: 35,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                showStickeers = true;
                              });
                            },
                            icon: const Icon(
                              Icons.emoji_emotions_outlined,
                              color: Colors.white,
                              size: 35,
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (showStickeers)
                      Positioned(bottom: 0, child: buildStickersContainer())
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      // bottomNavigationBar: Container(
      //   height: 60,
      //   width: size.width,
      //   color: Colors.blue,
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
      //     children: [],
      //   ),
      // ),
    );
  }

  Future<bool> showPopup() async {
    return await showDialog(
          //show confirm dialogue
          //the return value will be from "Yes" or "No" options
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Exit Editing'),
            content: Text('Do you want to discard editing?'),
            actions: [
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(false),
                //return false when click on "NO"
                child: Text('No'),
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(true),
                //return true when click on "Yes"
                child: Text('Yes'),
              ),
            ],
          ),
        ) ??
        false; //if
  }

  Future<bool> onBackPress() {
    if (showStickeers) {
      setState(() {
        showStickeers = false;
      });
    } else {
      Navigator.pop(context);
    }
    return Future.value(false);
  }

  Widget buildSticker(String index) {
    return TextButton(
      onPressed: () {
        File image = File('assets/stickers/mimi$index.gif');
        Key k = Key(_stackWidgets.length.toString());
        setState(() {
          _stackWidgets.add(
            ImageOverlayWidget(
              isAsset: true,
              photo: image,
              key: k,
              onDragStart: () {
                if (!_showDeleteButton) {
                  setState(() {
                    _showDeleteButton = true;
                  });
                }
              },
              onDragUpdate: (Offset offset, key) {
                if (offset.dy > MediaQuery.of(context).size.height - 50) {
                  if (!_isDeleteButtonActive) {
                    setState(() {
                      _isDeleteButtonActive = true;
                    });
                  }
                } else {
                  if (_isDeleteButtonActive) {
                    setState(() {
                      _isDeleteButtonActive = false;
                    });
                  }
                }
              },
              onDragEnd: (Offset offset, key) {
                if (_showDeleteButton) {
                  setState(() {
                    _showDeleteButton = false;
                  });
                }

                if (offset.dy > MediaQuery.of(context).size.height - 50) {
                  print(_stackWidgets.first.key);
                  _stackWidgets.removeWhere((element) => element.key == key);
                }
              },
            ),
          );
        });
      },
      child: Image.asset(
        'assets/stickers/mimi$index.gif',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget buildStickersContainer() {
    return Container(
      decoration: const BoxDecoration(
        border: Border(top: BorderSide(color: Colors.grey, width: 0.5)),
        color: Colors.black,
      ),
      // padding: EdgeInsets.all(5.0),
      // height: 180.0,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10, top: 10),
            child: IconButton(
              onPressed: () => setState(() {
                showStickeers = false;
              }),
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              buildSticker("1"),
              buildSticker("2"),
              buildSticker("3"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              buildSticker("4"),
              buildSticker("5"),
              buildSticker("6"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              buildSticker("7"),
              buildSticker("8"),
              buildSticker("9"),
            ],
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
