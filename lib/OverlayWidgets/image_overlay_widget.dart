import 'dart:io';

import 'package:editz/matrix_gesture_detector.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

typedef PointerMoveCallback = void Function(Offset offset, Key? key);

class ImageOverlayWidget extends StatelessWidget {
  final VoidCallback onDragStart;
  final PointerMoveCallback onDragEnd;
  final PointerMoveCallback onDragUpdate;
  final File photo;
  final isAsset;
  

  const ImageOverlayWidget({
    super.key,
    required this.onDragStart,
    required this.onDragEnd,
    required this.onDragUpdate,
    required this.photo,
    required this.isAsset
  });

  @override
  Widget build(BuildContext context) {
    final ValueNotifier<Matrix4> notifier = ValueNotifier(Matrix4.identity());
    late Offset offset;

    return Listener(
      onPointerMove: (event) {
        offset = event.position;
        onDragUpdate(offset, key);
      },
      child: MatrixGestureDetector(
        onMatrixUpdate: (matrix, translationDeltaMatrix, scaleDeltaMatrix,
            rotationDeltaMatrix) {
          notifier.value = matrix;
        },
        onScaleEnd: () {
          onDragEnd(offset, key);
        },
        onScaleStart: () {
          onDragStart();
        },
        onTap: () {},
        child: AnimatedBuilder(
          animation: notifier,
          builder: (context, childWidget) {
            return Transform(
              transform: notifier.value,
              child: Align(
                alignment: Alignment.center,
                // child: FittedBox(
                //   fit: BoxFit.contain,
                //   child: TextWidget(text: "Add Text"),
                // ),
                child: isAsset? 
                 Image.asset(
                  photo.path,
                  height: 100,
                  width: 100,
                ):Image.file(
                  photo,
                  height: 100,
                  width: 100,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
