import 'package:editz/matrix_gesture_detector.dart';
import 'package:flutter/material.dart';

typedef PointerMoveCallback = void Function(Offset offset, Key? key);

typedef TextStyleCallback = void Function(Key? key);

class TextOverlayWidget extends StatefulWidget {
  final VoidCallback onDragStart;
  final PointerMoveCallback onDragEnd;
  final PointerMoveCallback onDragUpdate;
  final TextStyleCallback onTaped;

  const TextOverlayWidget({
    super.key,
    required this.onDragStart,
    required this.onDragEnd,
    required this.onDragUpdate,
    required this.onTaped,
  });

  @override
  State<TextOverlayWidget> createState() => _TextOverlayWidgetState();
}

class _TextOverlayWidgetState extends State<TextOverlayWidget> {
  double fontSize = 30;

  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final ValueNotifier<Matrix4> notifier = ValueNotifier(Matrix4.identity());
    late Offset offset;

    return Listener(
      onPointerMove: (event) {
        offset = event.position;
        widget.onDragUpdate(offset, widget.key);
      },
      child: MatrixGestureDetector(
        onMatrixUpdate: (matrix, translationDeltaMatrix, scaleDeltaMatrix,
            rotationDeltaMatrix) {
          notifier.value = matrix;
        },
        onScaleEnd: () {
          widget.onDragEnd(offset, widget.key);
        },
        onScaleStart: () {
          widget.onDragStart();
        },
        onTap: () {
          print("tap detected");
          widget.onTaped(widget.key);
        },
        child: AnimatedBuilder(
          animation: notifier,
          builder: (context, childWidget) {
            return Transform(
              transform: notifier.value,
              child: Align(
                alignment: Alignment.center,
                // child: FittedBox(
                //   fit: BoxFit.contain,
                //   child: TextWidget(text: "Add Text"),
                // ),
                child: TextField(
                  onTap: () {
                    print("tap in text ${widget.key}");
                    widget.onTaped(widget.key);
                  },
                  controller: controller,
                  onSubmitted: (value) {
                    if (value.isNotEmpty) {
                      controller.text = value;
                    } else {}
                  },
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Add Text",
                      hintStyle:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w600)),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
