import 'package:editz/matrix_gesture_detector.dart';
import 'package:flutter/material.dart';

typedef PointerMoveCallback = void Function(Offset offset, Key? key);

class OverlayWidget extends StatelessWidget {
  final Widget child;
  final VoidCallback onDragStart;
  final PointerMoveCallback onDragEnd;
  final PointerMoveCallback onDragUpdate;
  final TextEditingController controller = TextEditingController();

  OverlayWidget({
    super.key,
    required this.child,
    required this.onDragStart,
    required this.onDragEnd,
    required this.onDragUpdate,
  });

  @override
  Widget build(BuildContext context) {
    print("overlay called");
    final ValueNotifier<Matrix4> notifier = ValueNotifier(Matrix4.identity());
    late Offset offset;

    return Listener(
      onPointerMove: (event) {
        offset = event.position;
        onDragUpdate(offset, key);
      },
      child: MatrixGestureDetector(
        onMatrixUpdate: (matrix, translationDeltaMatrix, scaleDeltaMatrix,
            rotationDeltaMatrix) {
          notifier.value = matrix;
        },
        onScaleEnd: () {
          onDragEnd(offset, key);
        },
        onScaleStart: () {
          onDragStart();
        },
        onTap: () {},
        child: AnimatedBuilder(
          animation: notifier,
          builder: (context, childWidget) {
            return Transform(
              transform: notifier.value,
              child: Align(
                alignment: Alignment.center,
                // child: FittedBox(
                //   fit: BoxFit.contain,
                //   child: TextWidget(text: "Add Text"),
                // ),
                child: TextWidget(controller: controller),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget textWidget() {
    return TextField(
      controller: controller,
      onSubmitted: (value) {
        if (value.isNotEmpty) {
          controller.text = value;
        } else {}
      },
      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Add Text",
          hintStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.w600)),
    );
  }
}

class TextWidget extends StatefulWidget {
  final TextEditingController controller;
  // String text;
  TextWidget({super.key, required this.controller});

  @override
  State<TextWidget> createState() => _TextWidgetState();
}

class _TextWidgetState extends State<TextWidget> {
  @override
  Widget build(BuildContext context) {
    print("called");
    return TextField(
      controller: widget.controller,
      onSubmitted: (value) => widget.controller.text = value,
      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Add Text",
          hintStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.w600)),
    );
  }
}
